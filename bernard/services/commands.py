import requests

import bernard.services.user as user


def infos() -> str:
    res = """
        Twitter=>https://twitter.com/Pesko_
        Gitlab=>https://gitlab.com/pesko
    """
    return res


def chuck() -> str:
    response = requests.get("https://api.chucknorris.io/jokes/random").json()
    return response["value"]


def velo() -> str:
    """Return informations about next event."""
    res = """
        Aucune sortie, j'attends la réception d'un nouveau micro.
    """
    return res


def help() -> str:
    """"Return a list with all our commands."""
    return """
    -> !infos : liens utiles.\u2800\u2800
    -> !chuck : mantra aléatoire de notre père à tous.\u2800\u2800
    -> !velo : prochaine sortie vélo sur la chaine.\u2800\u2800
    -> !status : afficher son statut dans le chat.\u2800\u2800
    -> !rank : afficher son rang dans le chat.\u2800\u2800
    -> !top : afficher les 5 bg du chat.\u2800\u2800
    """


def status(author: str) -> str:
    messages, status = user.get_messages_and_status(username=author)
    return f"@{author}, avec tes {messages} messages tu as le statut : {status}"


def get_rank(author: str) -> str:
    rank, messages = user.get_rank(author)
    if rank == 1:
        res = f"@{author}, tu es 1er avec {messages} messages ! Un grand bravo, amour et richesse te tendent les bras."
    elif rank == 2:
        res = f"@{author}, tu es bon 2ième avec {messages} messages! Tu talonnes le premier !"
    elif rank == 3:
        res = f"@{author}, c'est la 3ième position avec {messages} messages ! Ne reste pas dans l'ombre, accélère !"
    else:
        res = f"@{author}, tu es {rank}ième avec {messages} messages !"

    return res


def get_top_5() -> str:
    """Retrieve top 5 users."""
    top_five = list(user.get_all_ranks().items())[:5]
    res = ""
    for user_ in top_five:
        res += f"- {user_[0]} : {user_[1][0]} avec {user_[1][1]} messages."
    return res
