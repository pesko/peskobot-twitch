from peewee import CharField, IntegerField, Model, SqliteDatabase

PATH_DB = "/home/pesko/projects/peskobot/bernard.db"
DB = SqliteDatabase(PATH_DB)


class Base(Model):
    """A base model that will use our Sqlite database."""

    class Meta:
        database = DB


class User(Base):
    name = CharField()
    rank = IntegerField(default=0)
    messages = IntegerField(default=0)
    status = CharField(default="Nouveau")


def init_database():
    DB.connect()
    DB.create_tables([User])
